/**
 * Package containing various operators for generating Observables with numbers and filtering, querying and parsing
 * numbers in RxJS
 *
 * @packageDocumentation
 * @module Number
 *
 * @ignore
 */
/* istanbul ignore file */
export { add } from './lib/add';
export { div } from './lib/div';
export { filterInRange } from './lib/filter-in-range';
export { filterIsFinite } from './lib/filter-is-finite';
export { filterIsFloat } from './lib/filter-is-float';
export { filterIsInteger } from './lib/filter-is-integer';
export { filterIsNotNaN } from './lib/filter-is-not-nan';
export { filterIsSafeInteger } from './lib/filter-is-safe-integer';
export { filterOutOfRange } from './lib/filter-out-of-range';
export { fromFibonacci } from './lib/from-fibonacci';
export { fromNumber } from './lib/from-number';
export { inRange } from './lib/in-range';
export { isFinite } from './lib/is-finite';
export { isFloat } from './lib/is-float';
export { isInteger } from './lib/is-integer';
export { isNaN } from './lib/is-nan';
export { isNotNaN } from './lib/is-not-nan';
export { isSafeInteger } from './lib/is-safe-integer';
export { mod } from './lib/mod';
export { mul } from './lib/mul';
export { outOfRange } from './lib/out-of-range';
export { parseFloat } from './lib/parse-float';
export { parseHex } from './lib/parse-hex';
export { parseInt } from './lib/parse-int';
export { pow } from './lib/pow';
export { sub } from './lib/sub';
export { toExponential } from './lib/to-exponential';
export { toFixed } from './lib/to-fixed';
export { toHex } from './lib/to-hex';
export { toLocaleString } from './lib/to-locale-string';
export { toPrecision } from './lib/to-precision';
export { toString } from './lib/to-string';
