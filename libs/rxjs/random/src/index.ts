/**
 * Package containing various operators for generating Observables with random values in RxJS
 *
 * @packageDocumentation
 * @module Random
 *
 * @ignore
 */
/* istanbul ignore file */
export { fromRandom } from './lib/from-random';
export { fromRandomInt } from './lib/from-random-integer';
export { fromRandomStr } from './lib/from-random-string';
export { fromRandomCrypto } from './lib/from-random-crypto';
