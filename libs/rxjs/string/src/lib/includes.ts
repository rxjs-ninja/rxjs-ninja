/**
 * @packageDocumentation
 * @module String
 */
import { Observable, OperatorFunction } from 'rxjs';
import { map } from 'rxjs/operators';

/**
 * Returns an Observable that emits a boolean where the source string contains with the passed search string using
 * String.includes
 *
 * @category String Query
 *
 * @see The [[filterIncludes]] operator returns the string value
 *
 * @param searchStr The string to check the source ends with
 *
 * @example
 * Return a boolean where the source string includes 'JS'
 * ```ts
 * from(['RxJS', 'Ninja', 'Tests']).pipe(includes('JS')).subscribe();
 * ```
 * Output: `true, false, false`
 *
 * @returns Observable that emits a boolean
 */
export function includes(searchStr: string): OperatorFunction<string, boolean> {
  return (source: Observable<string>) => source.pipe(map((value) => value.includes(searchStr)));
}
