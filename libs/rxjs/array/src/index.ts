/**
 * Package containing various operators for filtering, querying and parsing arrays in RxJS
 *
 * @packageDocumentation
 * @module Array
 *
 * @ignore
 */
/* istanbul ignore file */
export { binarySearch } from './lib/binary-search';
export { difference } from './lib/difference';
export { differenceWith } from './lib/difference-with';
export { every } from './lib/every';
export { fill } from './lib/fill';
export { filterEvery } from './lib/filter-every';
export { filterSome } from './lib/filter-some';
export { find } from './lib/find';
export { findAll } from './lib/find-all';
export { findLast } from './lib/find-last';
export { findIndex } from './lib/find-index';
export { flipArray } from './lib/flip-array';
export { fromMap } from './lib/from-map';
export { fromObjectEntries } from './lib/from-object-entries';
export { fromObjectKeys } from './lib/from-object-keys';
export { fromSet } from './lib/from-set';
export { indexOf } from './lib/index-of';
export { intersects } from './lib/intersects';
export { intersectsWith } from './lib/intersects-with';
export { join } from './lib/join';
export { lastIndexOf } from './lib/last-index-of';
export { reverse } from './lib/reverse';
export { shuffle } from './lib/shuffle';
export { some } from './lib/some';
export { sort } from './lib/sort';
export { sortMap } from './lib/sort-map';
export { toSet } from './lib/to-set';

export { BinarySearchResult } from './types/binary-search';
